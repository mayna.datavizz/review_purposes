
from operator import le
from fastapi import FastAPI, Response, status, Path
from pydantic import BaseModel, Field
import pymysql.cursors

app = FastAPI()

user='root'
password='password'
db_name='catur'
host = '127.0.0.1'
connection = pymysql.connect(host=host,user=user,password=password,database=db_name, charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

class rankSchema(BaseModel):
    gelar: str = Field(..., min_length=0, max_length=255)
    deskripsi: str = Field(..., min_length=0, max_length=255)
    
@app.get("/")
def root():
    return "This is a script to connect Python and Database using PyMySQL and FastAPI"

@app.get("/api/view")
def view():   
    data = { "message": "db connected" }
    return data

@app.get("/api/showdb")
def showDB():
    cursor = connection.cursor()
    db_name = []
    cursor.execute("Show databases")
    for db in cursor:
        db_name.append(db)
    print(cursor.rowcount, "record(s) affected")
    cursor.close()
    return db_name
               
@app.get("/api/showtables")
def showTables():                                                   
    cursor=connection.cursor()
    tb_name = []
    cursor.execute("Show tables")                                 
    for tb in cursor:                                             
        tb_name.append(tb)                                                
    print(cursor.rowcount, "record(s) affected")
    cursor.close()
    return tb_name

@app.get("/api/showtables/{tb_name}")    
def showRows_of_rank(tb_name: str, response:Response):                                             
    cursor=connection.cursor()                                    
    sqlInput=f"SELECT * FROM {tb_name}"                               
    cursor.execute(sqlInput)                                      
    result=cursor.fetchall()
    if result is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": f"There is no data on {tb_name}", "status": response.status_code}                                     
    
    response = {"message": f"Succes to get the data from {tb_name}", "data": result}
    cursor.close()  
    return response

@app.delete("/api/delete_rank/{id}")
def deleteRow_in_rank(id: int, response:Response):                                  
    cursor=connection.cursor()
    sqlInput=f"DELETE FROM tb_rank where id=%s"
    data = (id,)                                                             
    cursor.execute(sqlInput,data)
    connection.commit()                                                                             
    response = {"message": f"Data deleted from table rank with id: {id}"}
    cursor.close()
    return response                                           
       
@app.post("/api/insert_rank/{item_id}")
def insert_into_rank(
    item_id: int = Path(..., title="Item", ge=0, le=1000),
    data: rankSchema = None,
):                                     
    cursor=connection.cursor()
    sqlInput="INSERT into tb_rank (id,gelar,deskripsi) values (%s,%s,%s)"   
    data=(item_id, data.gelar, data.deskripsi)                                               
    cursor.execute(sqlInput,data)
    connection.commit()
    cursor.close()
    return {"message": "1 row inserted"}
    
@app.put("/api/update_rank/{item_id}")
def updateRank(
    item_id: int = Path(..., title="Item", ge=0, le=1000),
    data: rankSchema = None,
):                                                   
    cursor = connection.cursor()              
    sqlInput = f"UPDATE tb_rank SET gelar = %s WHERE id = %s"
    data=(data.gelar, item_id)
    cursor.execute(sqlInput, data)                                      
    connection.commit()                                             
    print(cursor.rowcount, "record(s) affected")
    cursor.close()
    return {"message": "1 row updated"}
                                                                                                   