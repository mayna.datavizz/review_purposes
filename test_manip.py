# Disclaimer:
# Edited from: https://realpython.com/working-with-files-in-python/ 

import os
import fnmatch
import glob
from tempfile import TemporaryFile
import fileinput
import sys

# declaring path into my directory
my_dir_path = 'C:/Users/Mayna/datavizz/'

# MANIPULATING FILES
# write data
with open('manipulates/manipulate_me.txt', 'w') as file:
    print('This will be written to manipulates/manipulate_me.txt', file=file)
    print('Will it work?', file=file)
    
# write data and just curious about reversed
with open('manipulates/spam.txt', 'w') as file:
    file.write('Spam and eggs!')
    numb = ['1', '2', '3', '4', '5', '6', '10']
    text = reversed(numb)
    file.write(str(text))
    
# adding new data    
with open('manipulates/manipulate_me.txt', 'r+') as file:
     old = file.read() # read everything in the file
     file.seek(0) # rewind
     file.write('Adding a new line in the beginning\n' + old) # write the new line in the beginning
     file.write(old + 'Adding a new line at the end of the document\n') # write the new line in the end
    
# just another way to write something into file
# with open('manipulates/manipulate_me.txt', 'w') as f:
#    data = 'just adding another line'
#    f.write(data)

# updating data
# replace all occurrences of 'Adding' with 'Deleting'
for i, line in enumerate(fileinput.input('manipulates/manipulate_me.txt', inplace=1)):
    sys.stdout.write(line.replace('Adding', 'Deleting'))  # replace 'adding' and write
    if i == 2: sys.stdout.write('\n')  # write a blank line after the 3rd line
    
# deleting data    
with open('manipulates/manipulate_me.txt', 'r') as file:
    lines = file.readlines()
with open('manipulates/manipulate_me.txt', 'w') as file:
    for line in lines:
        if line.strip("\n") != "Will it work?":
            file.write(line)
    
# reading data    
with open('manipulates/manipulate_me.txt', 'r') as file:
    print("Data from manipulate_me.txt: ")
    data = file.read()
    print(data)   

# MANIPULATING DIRECTORIES
entries = os.listdir(my_dir_path)
print("List of files in directory using listdir: ")
for entry in entries:
    print(entry)
    
# using scandir
with os.scandir(my_dir_path) as entries:
    print("files and directories using scandir: ")
    for entry in entries:
        print(entry.name)
        
# trying to reach subdir 'manipulates'
with os.scandir(my_dir_path + '/manipulates') as entries:
    print("files in sub directory: ")
    for entry in entries:
        print(entry.name)
        
# filter out directory and only listing the files 
for entry in os.listdir(my_dir_path):
    if os.path.isfile(os.path.join(my_dir_path, entry)):
        print("files only in directory: ")
        print(entry)
        
# List all subdirectories using scandir() and is_dir()
with os.scandir(my_dir_path) as entries:
    print("The list of dir using is_dir(): ")
    for entry in entries:
        if entry.is_dir():
            print(entry.name)
            
# get .txt files
print("Files with .txt extension on sub directory are: ")
for f_name in os.listdir(my_dir_path + '/manipulates'):
    if f_name.endswith('.txt'):
        print(f_name)

print("result using fnmatch: ")        
for f_name in os.listdir(my_dir_path + '/manipulates'):
    if fnmatch.fnmatch(f_name, '*.txt'):
        print(f_name)
         
# using glob
print("results using glob: ")
for f_name in glob.glob('*.ipynb'):
    print(f_name)
    
# using glob recursively
print("results using glob recursively to sub directory: ")
for f_name in glob.iglob('**/*.py', recursive=True):
    print(f_name)
    

# TEMPORARY FILES
# Create a temporary file and write some data to it
temp_file = TemporaryFile('w+t')
temp_file.write('Hello universe!')

# Go back to the beginning and read data from file
temp_file.seek(0)
data = temp_file.read()
print(data)

# Close the file, after which it will be removed
temp_file.close()

