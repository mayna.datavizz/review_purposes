import fnmatch
from pathlib import Path
import pdfkit
import pandas as pd
import convertapi
import sys, os
import fpdf
sys.path.append("..")


# The files 
path = Path('./review_purposes')
path = 'review_purposes/manipulated_b.csv'


# Just changing the file's name
def change_file_type():      
    for file in path.iterdir():
        if file.is_file() and file.suffix in ['.pdf'] and fnmatch.fnmatch(file, '*_b.pdf'):
            file.rename(file.with_suffix('.csv'))


# This function work: using pandas
def convert_csv_pdf():                        
    file = pd.read_csv(path)  
    file.to_html('review_purposes/manipulated_b.html')
    # configure the path to wkhtmltopdf
    path_wk=r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=path_wk)
    # converting html files into pdf
    pdfkit.from_file('review_purposes/manipulated_b.html','review_purposes/converted_pandas.pdf', configuration=config)


# This function work: using API
def convert_API():
    convertapi.api_secret = 'e73YPOvEVkepohrr'
    result = convertapi.convert(
    'pdf',
    { 'File': path },
    from_format = 'csv'
)
    result.file.save('review_purposes/converted_API.pdf')

  
# Using PyFPDF
pdf = fpdf.FPDF()
pdf.add_page();
pdf.set_font('Arial','B', 12);
with open(path) as file:
   for line in file.readlines():
      print(line)
      pdf.write(10, txt= str(line))

pdf.output(r'review_purposes/converted_FPDF.pdf','F')
import sys
if sys.platform.startswith('linux'):
    os.system('xdg-open ./invoice.pdf')
else:
    os.system('review_purposes/converted_FPDF.pdf')
    
convert_csv_pdf()
convert_API()


    


         