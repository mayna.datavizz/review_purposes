import csv

# Files:
file_path = 'review_purposes/biostats.csv'
manipulate = 'review_purposes/manipulated_a.csv'
manipulate_b = 'review_purposes/manipulated_b.csv'


def readCSV():
    with open(file_path, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in reader:
            #print(', '.join(row))
            print(row)
        
        
def writeCSV():
    with open(file_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['mayna'] * 4 + ['nami'])
        writer.writerow(['mayna', 'hurry', 'nami'])
        

def read_dictCSV():    
    with open(manipulate_b, newline='') as csvfile:
        reader = csv.DictReader(csvfile, skipinitialspace=True)
        for row in reader:
            print(row['Name'], row['Sex'], row['Age'], row['Height (in)'], row['Weight (lbs)'])


def write_dictCSV():
    with open(manipulate_b, 'w', newline='') as csvfile:
        fieldnames = ['Name', 'Sex', 'Age', 'Height (in)', 'Weight (lbs)']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, skipinitialspace=True)
        writer.writeheader()
        writer.writerow({'Name': 'Mayna', 'Sex': 'F', 'Age': '22', 'Height (in)': '60', 'Weight (lbs)': '88'})
        
        
def appendCSV():
    with open (manipulate_b, 'a', newline='') as csvfile:
        fieldnames = ['Name', 'Sex', 'Age', 'Height (in)', 'Weight (lbs)']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, skipinitialspace=True)
        writer.writerow({'Name': 'Hurry', 'Sex': 'F', 'Age': '22', 'Height (in)': '60', 'Weight (lbs)': '88'})
        writer.writerow({'Name': 'Nami', 'Sex': 'F', 'Age': '22', 'Height (in)': '60', 'Weight (lbs)': '88'})
        writer.writerow({'Name': 'Mama', 'Sex': 'F', 'Age': '22', 'Height (in)': '60', 'Weight (lbs)': '88'})
        writer.writerow({'Name': 'Abah', 'Sex': 'F', 'Age': '22', 'Height (in)': '60', 'Weight (lbs)': '88'})

            
def updateCSV():
    lines=list()
    old_name='Hurry'
    new_name='Mayna'
    new_sex='F'
    new_age='22'
    new_height='150'
    new_weight='40'   
    fieldnames = ['Name', 'Sex', 'Age', 'Height (in)', 'Weight (lbs)']
    with open(manipulate_b, 'r', newline='') as input_file:
        reader = csv.DictReader(input_file, fieldnames=fieldnames, skipinitialspace=True)
        for row in reader:
            if row['Name'] == str(old_name):
                row['Name'], row['Sex'], row['Age'], row['Height (in)'], row['Weight (lbs)'] = new_name, new_sex, new_age, new_height, new_weight
            row = {'Name': row['Name'], 'Sex': row['Sex'], 'Age' : row['Age'], 'Height (in)': row['Height (in)'], 'Weight (lbs)' : row['Weight (lbs)']}
            lines.append(row)
    with open(manipulate_b, 'w', newline='') as output_file:
        writer = csv.DictWriter(output_file, fieldnames=fieldnames, skipinitialspace=True)      
        writer.writerows(lines) 
        
  
def deleteCSV():
    row_to_remove=[2]
    lines = list()
    with open(manipulate_b, 'r', newline='') as input_file:
        reader = csv.reader(input_file)
        for row_number, row in enumerate(reader, start=1):
            if(row_number not in row_to_remove):
                lines.append(row)
                
    with open(manipulate_b, 'w', newline='') as output_file:
        writer = csv.writer(output_file)
        writer.writerows(lines)
        
        
#write_dictCSV()
#appendCSV()
#deleteCSV()
#updateCSV()      
#read_dictCSV()